LEVEL_MAP = [
    '                                                                                   ',
    '                                                                                   ',
    '                                                                                   ',
    '                                                                                   ',
    '                                                                                   ',
    '          X              X                            X                            ',
    'XXXXXXXXXXXXX  XXXXXXXXXXXXX  XX           X          X                            ',
    'X                                XX     XXXX          X                            ',
    'X           X              X          XXXXXX  XX      X XXX                        ',
    'X P        XX             XX        XXXXXXXX  XXXX                                 ',
    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX']

ALPHA_COLOR = "#EF18C5"
BG_COLOR = "#3C3F41"
TILE_COLOR = "#AFB1B3"

TILE_SIZE = 64
WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

DAMAGE = 3


CAMERA_BORDERS = {"top_margin": 100,
                  "left_margin": 600,
                  "right_margin": 600,
                  "bottom_margin": 200}
