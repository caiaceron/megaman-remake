import pygame as pg
from settings import TILE_SIZE, TILE_COLOR


class Tile(pg.sprite.Sprite):
    def __init__(self, position, groups):
        super().__init__(groups)
        self.image = pg.surface.Surface((TILE_SIZE, TILE_SIZE))
        self.image.fill(TILE_COLOR)
        self.rect = self.image.get_rect(topleft=position)
