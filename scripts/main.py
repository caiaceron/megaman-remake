# IMPORT PACKAGE
import sys
import pygame as pg
from settings import *
from level import Level

# General setup
game_is_running = True
fps = 60

pg.init()
window = pg.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pg.display.set_caption("Megaman I Remake")
clock = pg.time.Clock()
level = Level()

while game_is_running:

    for event in pg.event.get():
        if event.type == pg.QUIT:
            pg.quit()
            sys.exit()

    window.fill(BG_COLOR)
    level.run()
    pg.display.update()
    clock.tick(fps)
