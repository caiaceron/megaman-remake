import pygame as pg
from settings import CAMERA_BORDERS


class CameraGroup(pg.sprite.Group):
    def __init__(self):
        super().__init__()
        self.display_surface = pg.display.get_surface()
        self.offset = pg.math.Vector2(100, 300)

        # Camera Param
        cam_top = CAMERA_BORDERS['top_margin']
        cam_left = CAMERA_BORDERS['left_margin']
        cam_width = self.display_surface.get_size()[0] - (cam_left + CAMERA_BORDERS['right_margin'])
        cam_height = self.display_surface.get_size()[1] - (cam_top + CAMERA_BORDERS['bottom_margin'])

        self.camera_rect = pg.Rect(cam_left, cam_top, cam_width, cam_height)

    def custom_draw(self, player):
        # trying to get the player offset
        if player.rect.left < self.camera_rect.left:
            self.camera_rect.left = player.rect.left
        if player.rect.right > self.camera_rect.right:
            self.camera_rect.right = player.rect.right
        if player.rect.top < self.camera_rect.top:
            self.camera_rect.top = player.rect.top
        if player.rect.bottom > self.camera_rect.bottom:
            self.camera_rect.bottom = player.rect.bottom

        self.offset = pg.Vector2(self.camera_rect.left - CAMERA_BORDERS['left_margin'],
                                 self.camera_rect.top - CAMERA_BORDERS['top_margin'])

        for sprite in self.sprites():
            offset_pos = sprite.rect.topleft - self.offset
            self.display_surface.blit(sprite.image, offset_pos)