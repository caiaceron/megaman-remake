import pygame as pg


class Projectile:
    def __init__(self, position, damage):
        self.image = pg.Surface((2, 2))
        self.image.fill("#F8DA51")
        self.rect = self.image.get_rect(topleft=position)
        self.cu = self.rect

        self.damage = damage
        self.bullet_spawn = position
        self.bullet_speed = 4
        self.direction = pg.math.Vector2(0, 0)
