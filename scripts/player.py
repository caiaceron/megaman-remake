import pygame as pg
from projectile import Projectile

class Player(pg.sprite.Sprite):
    def __init__(self, position, groups, collision_sprites):
        super().__init__(groups)
        self.image = pg.Surface((34, 40))
        self.image.fill("#30C0D8")  # Megaman Blue color code in Hex
        self.rect = self.image.get_rect(topleft=position)

        # Player movement
        self.direction = pg.math.Vector2(0, 0)
        self.speed = 3.5
        self.gravity_force = 0.8
        self.jump_force = -16
        self.collision_sprites = collision_sprites
        self.on_the_ground = False
        self.face_right = True
        self.face_left = False

    def input(self):
        buttons = pg.key.get_pressed()

        if buttons[pg.K_RIGHT] and not buttons[pg.K_LEFT]:
            self.direction.x = 1
            self.face_right = True
            self.face_left = False

        elif buttons[pg.K_LEFT] and not buttons[pg.K_RIGHT]:
            self.direction.x = -1
            self.face_left = True
            self.face_right = False

        else:
            self.direction.x = 0

        # Jump Button
        if buttons[pg.K_x] and self.on_the_ground == True:
            self.jumping()

        # Mega Buster Button
        if buttons[pg.K_c]:
            self.shoot()

        # Slide Button
        if buttons[pg.K_z]:
            print("Slide")

    def gravity(self):
        self.direction.y += self.gravity_force
        self.rect.y += self.direction.y

    def jumping(self):
        self.direction.y += self.jump_force
        self.rect.y += self.direction.y

    def vertical_collision(self):
        for sprite in self.collision_sprites.sprites():
            if sprite.rect.colliderect(self.rect):
                if self.direction.y < 0:
                    self.rect.top = sprite.rect.bottom
                    self.direction.y = 0

                if self.direction.y > 0:
                    self.rect.bottom = sprite.rect.top
                    self.direction.y = 0
                    self.on_the_ground = True
            if self.on_the_ground == True and self.direction.y != 0:
                self.on_the_ground = False

    def horizontal_collision(self):
        for sprite in self.collision_sprites.sprites():
            if sprite.rect.colliderect(self.rect):
                if self.direction.x < 0:
                    self.rect.left = sprite.rect.right
                if self.direction.x > 0:
                    self.rect.right = sprite.rect.left

    def shoot(self):
        print("Lemons!!!")

    def update(self):
        self.input()
        self.rect.x += self.direction.x * self.speed
        self.horizontal_collision()
        self.gravity()
        self.vertical_collision()
        print("Right Side: ", self.face_right, "Left Side: ", self.face_left)
