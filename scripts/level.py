import pygame as pg
from player import Player
from tiles import Tile
from settings import TILE_SIZE, LEVEL_MAP
from camera import CameraGroup


class Level:
    def __init__(self):
        self.display_surface = pg.display.get_surface()

        self.visible_sprites = CameraGroup()
        self.active_sprites = pg.sprite.Group()
        self.collision_sprites = pg.sprite.Group()

        self.level_builder()

    def level_builder(self):
        for row_index, row in enumerate(LEVEL_MAP):
            for column_index, column in enumerate(row):
                x = column_index * TILE_SIZE
                y = row_index * TILE_SIZE
                if column == "X":
                    # Create the tiles Objects
                    Tile((x, y), [self.visible_sprites, self.collision_sprites])
                if column == "P":
                    # Create Player Object
                   self.player = Player((x, y), [self.visible_sprites, self.active_sprites], self.collision_sprites)

    def run(self):
        self.collision_sprites.update()
        self.active_sprites.update()
        self.visible_sprites.custom_draw(self.player)
